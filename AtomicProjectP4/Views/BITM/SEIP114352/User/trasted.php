<a href="create.php">Create     |</a></br>
<a href="index.php">See All User</a>


<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\User\User;

session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset ($_SESSION['Message']);
}

$data= new User();
$Alluser=$data->trasted();
?>
<html>
    <title>All user</title>
    <body>
        <table border="1">
            <tr>
                <th>SL</th>
                <th>User Name</th>
                <th>Action</th>
            </tr>
            <?php
            if(!empty($Alluser)){
                $serial=0;
                foreach ($Alluser as $Oneuser) {
                    $serial++;
            ?>
            <tr>
                <td><?php echo $serial; ?></td>
                <td><?php echo $Oneuser['title'];?></td>
                <td>
                    <a href="restore.php?id=<?php echo $Oneuser['id']?>">Restore</a>
                    <a href="delete.php?id=<?php echo $Oneuser['id']?>">Delete</a>
                </td>
            </tr>
            <?php
                }
            }
            else{
            ?>
            <tr>
                <td colspan="3">No available data</td>
            </tr>
            <?php
            }
            ?>
            
        </table>
        
    </body>
    
</html>


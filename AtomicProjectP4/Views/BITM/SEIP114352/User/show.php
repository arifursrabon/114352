<a href="create.php">Create</a>

<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\User\User;

session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset ($_SESSION['Message']);
}

$data= new User();
$data->prepare($_GET);
$Oneuser=$data->show();
?>
<html>
    <title>All user</title>
    <body>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>User Password</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Action</th>
            </tr>
            <?php
            if(!empty($Oneuser)){
               
            ?>
            <tr>
                <td><?php echo $Oneuser['id']; ?></td>
                <td><?php echo $Oneuser['title'];?></td>
                <td><?php echo $Oneuser['password'];?></td>
                <td><?php echo $Oneuser['created'];?></td>
                <td><?php echo $Oneuser['updated'];?></td>
                <td>
                    <a href="edit.php?id=<?php echo $Oneuser['id']?>">Edit</a>
                    <a href="trast.php?id=<?php echo $Oneuser['id']?>">Delete</a>
                </td>
            </tr>
            <?php
                }
            else{
            ?>
            <tr>
                <td colspan="6">No available data</td>
            </tr>
            <?php
            }
            ?>
            
        </table>
        
    </body>
    
</html>


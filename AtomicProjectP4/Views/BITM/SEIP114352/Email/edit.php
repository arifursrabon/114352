<a href="create.php">Create</a>

<?php
error_reporting(E_ALL ^ E_DEPRECATED);

include_once"../../../../vendor/autoload.php";

use App\BITM\SEIP114352\Email\Email;
use App\BITM\SEIP114352\Utility\Utility;

$data = new Email();
$data->prepare($_GET);
$Oneemail = $data->show();
?>

<html>
    <head>
        <title>Update | Email</title>
    </head>
    <body>
        <fieldset>
            <legend>
                Update Email
            </legend> 
            <form action="update.php" method="POST">
                <label>Update Your Email Address</label><br/>
                <input type="email" name="title" id="title" value="<?php echo $Oneemail['title']; ?>"><br/>
                <input type="submit" value="Save">
                <input type="hidden" name="id" value="<?php echo $Oneemail['id']; ?>">
            </form>
        </fieldset>
    </body>
</html>
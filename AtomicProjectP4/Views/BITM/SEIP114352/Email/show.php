<a href="create.php">Create</a>

<?php
error_reporting(E_ALL^E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Email\Email;

$data = new Email();
$data->prepare($_GET);
$Oneemail = $data->show();

?>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Created</th>
        <th>Modified</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $Oneemail['id']; ?></td>
        <td>
            <?php
            if(isset($Oneemail['title']) && !empty($Oneemail['title'])){
            echo $Oneemail['title'];
            }
            else{
                echo "No Available Data";
            }
            ?>
        </td>
        <td><?php echo $Oneemail['created']; ?></td>
        <td><?php echo $Oneemail['updated']; ?></td>
        <td>
            <a href="edit.php?id=<?php echo $Oneemail['id'] ?>">Edit</a>  |
            <a href="delete.php?id=<?php echo $Oneemail['id']; ?>">Delete</a> 
            
        </td>
        
    </tr>
    
    
    
</table>
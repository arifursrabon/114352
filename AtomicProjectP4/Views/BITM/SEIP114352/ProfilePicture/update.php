<?php

error_reporting(E_ALL ^ E_DEPRECATED);
include_once"../../../../vendor/autoload.php";
use App\BITM\SEIP114352\ProfilePicture\ProfilePicture;

$data = new ProfilePicture();
$data->prepare($_POST);
$Onedata = $data->show();

if (isset($_FILES['image'])) {
    $errors = array();
    $file_name = time() . $_FILES['image']['name'];
    $file_size = $_FILES['image']['size'];
    $file_tmp = $_FILES['image']['tmp_name'];
    $file_type = $_FILES['image']['type'];
    $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));

    $format = array("jpeg", "jpg", "png");

    if (in_array($file_ext, $format) === false) {
        $errors[] = "extention not allowed, please choose a JPEG or PNG file.";
    }
    if ($file_size > 2097152) {
        $errors[] = "File size must be exactly 2MB.";
    }
    if (empty($errors) == true) {
        unlink($_SERVER['DOCUMENT_ROOT'].'/114352/AtomicProjectP4/Image/'.$Onedata['image']);
        move_uploaded_file($file_tmp, "../../../../Image/" . $file_name);
        $_POST['image'] = $file_name;
    } else {
        print_r($errors);
    }
}
$data->prepare($_POST);
$data->update();
?>

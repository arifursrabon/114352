<?php

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\City\City;

$data = new City();
$data->prepare($_GET);
$Onedata = $data->show();
?>
<html>
    <title>City</title>
    <body>
        <fieldset>
            <legend>Update</legend>
            <form action="update.php" method="POST">
                <label>Update your name</label>
                <input type="text" name="name" value="<?php echo $Onedata['name']?>"><br/>
                <label >Select City </label><br/>
                <select name="city">
                    <option value="Dhaka" <?php if($Onedata['city'] == 'Dhaka'){?>selected<?php }?>>Dhaka</option>
                    <option value="Comilla" <?php if($Onedata['city'] == 'Comilla'){?>selected<?php }?>>Comilla</option>
                    <option value="Chuadanga" <?php if($Onedata['city'] == 'Chuadanga'){?>selected<?php }?> >Chuadanga</option>
                    <option value="Bogra" <?php if($Onedata['city'] == 'Bogra'){?>selected<?php }?>>Bogra</option>
                    <option value="Savar" <?php if($Onedata['city'] == 'Savar'){?>selected<?php }?>>Savar</option>
                    <option value="Rangpur" <?php if($Onedata['city'] == 'Rangpur'){?>selected<?php }?>>Rangpur</option>
                </select></br>
                <input type="submit" value="Update" name="submit">
                <input type="hidden" name="id" value="<?php echo $Onedata['id']?>">
            </form>
            
        </fieldset>  
    </body>
</html>
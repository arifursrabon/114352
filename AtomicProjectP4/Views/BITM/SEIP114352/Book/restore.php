<?php

error_reporting(E_ALL^E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Book\Book;

$restore = new Book();
$restore->prepare($_GET);
$restore->restore();
?>

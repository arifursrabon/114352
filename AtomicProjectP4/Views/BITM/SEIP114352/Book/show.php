<a href="create.php">Create</a>

<?php
error_reporting(E_ALL^E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Book\Book;

$book = new Book();
$book->prepare($_GET);
$Onebook = $book->show();

?>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Author</th>
        <th>Created</th>
        <th>Modified</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $Onebook['id']; ?></td>
        <td>
            <?php
            if(isset($Onebook['title']) && !empty($Onebook['title'])){
            echo $Onebook['title'];
            }
            else{
                echo "No Available Data";
            }
            ?>
        </td>
        <td>
            <?php
            if(isset($Onebook['author']) && !empty($Onebook['author'])){
            echo $Onebook['author'];
            }
            else{
                echo "No Available Data";
            }
            ?>
        </td>
        <td><?php echo $Onebook['created']; ?></td>
        <td><?php echo $Onebook['updated']; ?></td>
        <td>
            <a href="edit.php?id=<?php echo $Onebook['id'] ?>">Edit</a> |
            <a href="delete.php?id=<?php echo $Onebook['id']; ?>">Delete</a> 
            
        </td>
        
    </tr>
    
    
    
</table>
<?php

include_once "../../../../vendor/autoload.php";
//include_once '../Utility/Utility.php';

use App\BITM\SEIP114352\Book\Book;
use App\BITM\SEIP114352\Utility\Utility;

$books = new Book();
$books->prepare($_POST);
$books->store();

//$objForDebug = new Utility();
//$objForDebug->debug($data);
?>
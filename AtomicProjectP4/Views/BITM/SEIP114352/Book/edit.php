<?php
error_reporting(E_ALL^E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Book\Book;

$book = new Book();
$book->prepare($_GET);
$Onebook = $book->show();

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

?>
<html>
    <title>Update book list</title>
    <body>
        <fieldset>
            <legend>Update Your Favorite Book</legend>
            <form action="update.php" method="POST">
                <label for="title"> Update Book Name </label><br/>
                <input type="text" name="title" id="title" value="<?php echo $Onebook['title'] ?>"><br/>
                <label for="author"> Update Author Name </label><br/>
                <input type="text" name="author" id="author" value="<?php echo $Onebook['author'] ?>"><br/>
                <input type="hidden" name="id" value="<?php echo $Onebook['id']  ?>">
                <input type="submit" value="Save" name="submit">

                
            </form>
            
        </fieldset>
        
        
        
    </body>
</html>
<a href="create.php"><b>Create</b></a>

<?php
error_reporting(E_ALL^E_DEPRECATED);
include_once "../../../../vendor/autoload.php";
use App\BITM\SEIP114352\Book\Book;

session_start();
if(isset($_SESSION['Message'])){
    echo ($_SESSION['Message']);
    unset($_SESSION['Message']);
}

$books = new Book();
$Allbooks = $books->trasted();


?>
<html>
    <table border="1">
        <tr>
            <th>SL</th>
            <th>Title</th>
            <th>Author</th>
            <th>Action</th>
        </tr>
        <?php
        if(isset($Allbooks)&& !empty($Allbooks)){
            $serial = 0;
            foreach ($Allbooks as $Onebooks) {
                $serial++;
        ?>
        <tr>
            <td><?php echo $serial ?></td>
            <td><?php echo $Onebooks['title'] ?></td>
            <td><?php echo $Onebooks['author'] ?></td>
            <td>
                <a href="restore.php?id=<?php echo $Onebooks['id'] ?>">Restore</a>    |
                <a href="delete.php?id=<?php echo $Onebooks['id'] ?>">Delete</a>    
            </td>   
        </tr>
        <?php
            }
        }
        else{
        ?>
        <tr>
            <td colspan="4"><?php echo "No availave data "; ?></td>
        </tr>
        <?php
        }
        ?>
        
    </table>
       
</html>
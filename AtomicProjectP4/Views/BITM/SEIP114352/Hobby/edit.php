<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Hobby\Hobby;

$data =new Hobby();
$data->prepare($_GET);
$Onehobby= $data->show();
?>

<html>
    <head>
        <title>Update</title>
    </head>
    <fieldset>
        <legend>Update your hobbies</legend>
        <form  action="update.php" method="POST">
            <label>Select your hobbies</label></br>
            <input type="checkbox" name="name[]" value="Cricket" id="Cricket"
            <?php if(preg_match("/Cricket/", $Onehobby['name'])){ echo "checked";} else{echo '';}?>/>Cricket</br>
            <input type="checkbox" name="name[]" value="Football" id="Football"
            <?php if(preg_match("/Football/", $Onehobby['name'])){ echo "checked";} else{echo '';}?>/>Football</br>
            <input type="checkbox" name="name[]" value="Chase" id="Chase"
            <?php if(preg_match("/Chase/", $Onehobby['name'])){ echo "checked";} else{echo '';}?>/>Chase</br>
            <input type="checkbox" name="name[]" value="Gardening" id="Gardening"
            <?php if(preg_match("/Gardening/", $Onehobby['name'])){ echo "checked";} else{echo '';}?>/>Gardening</br>
            <input type="checkbox" name="name[]" value="Art" id="Art"
            <?php if(preg_match("/Art/", $Onehobby['name'])){ echo "checked";} else{echo '';}?>/>Art</br>
            <input type="hidden" name="id" value="<?php echo $Onehobby['id'] ?>"></br>
            <input type="submit" value="Update">
        </form>
        
    </fieldset>
</html>

<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Hobby\Hobby;

$data =new Hobby();
$data->prepare($_GET);
$Onehobby= $data->show();
?>

<html>
    <body>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Hobbies</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Action</th>
            </tr>
            <?php
            if(isset($Onehobby)&& !empty($Onehobby)){
            ?>
            <tr>
                <td><?php echo $Onehobby['id'] ?></td>
                <td><?php echo $Onehobby['name'] ?></td>
                <td><?php echo $Onehobby['created'] ?></td>
                <td><?php echo $Onehobby['updated'] ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $Onehobby['id'] ?>">Edit    |</a>
                    <a href="trast.php?id=<?php echo $Onehobby['id'] ?>">Delete</a>
                </td>
            </tr>
            <?php 
            }
            else { 
                ?>
            <tr>
                <td colspan="5"><?php echo "No available data" ?></td>
            </tr>
            <?php
            }
            ?>
                        
        </table>
    </body>
</html>
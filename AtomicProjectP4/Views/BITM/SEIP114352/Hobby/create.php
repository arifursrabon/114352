<?php
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
    
}

?>

<html>
    <head>
        <title>Create</title>
    </head>
    <fieldset>
        <legend>CRUD    | Checkbox (Multiple)</legend>
        <form  action="store.php" method="POST">
            <label>Select your hobbies</label></br>
            <input type="checkbox" name="name[]" value="Cricket" id="Cricket">Cricket</br>
            <input type="checkbox" name="name[]" value="Football" id="Football">Football</br>
            <input type="checkbox" name="name[]" value="Chase" id="Chase">Chase</br>
            <input type="checkbox" name="name[]" value="Gardening" id="Gardening">Gardening</br>
            <input type="checkbox" name="name[]" value="Art" id="Art">Art</br>
            <input type="submit" value="Save">
            <input type="reset" value="Reset">
        </form>
        
    </fieldset>
</html>
<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Hobby\Hobby;
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
    
}

$data= new Hobby();
$allhobbies=$data->trasted();
?>

<html>
    <body>
        <table border="1" style="">
            <tr>
                <th>SL</th>
                <th>Hobbies</th>
                <th>Action</th>
            </tr>
            <?php
            if(isset($allhobbies) && !empty($allhobbies)){
                $serial=0;
            foreach ($allhobbies as $Onehobby) {
                $serial++;
            ?>
            <tr>
                <td><?php echo $serial; ?></td>
                <td><?php echo $Onehobby['name'] ?></td>
                <td>
                    <a href="restore.php?id=<?php echo $Onehobby['id'] ?>">Restore    |</a>
                    <a href="delete.php?id=<?php echo $Onehobby['id'] ?>">Delete</a>
                </td>
            </tr>
            <?php 
                }
            }
            else { 
                ?>
            <tr>
                <td colspan="3"><?php echo "No available data" ?></td>
            </tr>
            <?php
            }
            ?>
                        
        </table>
    </body>
</html>
<a href="create.php">Create</a>

<?php
error_reporting(E_ALL^E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Terms\Terms;

$data = new Terms();
$data->prepare($_GET);
$Onename = $data->show();

?>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Created</th>
        <th>Modified</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $Onename['id']; ?></td>
        <td>
            <?php
            if(isset($Onename['name']) && !empty($Onename['name'])){
            echo $Onename['name'];
            }
            else{
                echo "No Available Data";
            }
            ?>
        </td>
        <td><?php echo $Onename['created']; ?></td>
        <td><?php echo $Onename['updated']; ?></td>
        <td>
            <a href="edit.php?id=<?php echo $Onename['id'] ?>">Edit</a>  |
            <a href="delete.php?id=<?php echo $Onename['id']; ?>">Delete</a> 
            
        </td>
        
    </tr>
    
    
    
</table>
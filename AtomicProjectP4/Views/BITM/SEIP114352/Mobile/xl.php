<?php

error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI=='cli'){
    die ('This example should only be run from a Web Browser');
}
require_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Mobile\Mobile;

$mobiles = new Mobile();
$Allmobiles = $mobiles->index();

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Meerten Balliauw")
        ->setLastModifiedBy("Meerten Balliauw")
        ->setTitle("Ofice 2007 XLSX Text Document")
        ->setSubject("Ofice 2007 XLSX Text Document")
        ->setDescription("text document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("test result file");

$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'SL')
        ->setCellValue('B1', 'ID')
        ->setCellValue('C1', 'Title');
$counter= 2;
$serial=0;
foreach ($Allmobiles as $Onemobiles) {
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$counter, $serial)
        ->setCellValue('B'.$counter, $Onemobiles['id'])
        ->setCellValue('C'.$counter, $Onemobiles['title']);
    $counter++;
    
}
$objPHPExcel->getActiveSheet()->setTitle('Mobile_List');
$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.ms-exel');
header('Content-Disposition: attachment;filename="01simple.xls" ');
header('cache-Control: max-age=0');
header('catche-Control: max-age=1');

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pagma: public');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

<?php
include_once "../../../../vendor/autoload.php";

use App\BITM\SEIP114352\Mobile\Mobile;
use App\BITM\SEIP114352\Utility\Utility;

$mobiles = new Mobile();
$data =$mobiles->prepare($_POST);
$mobiles->store();

$objForDebug = new Utility();
$objForDebug->debug($data);
?>

<a href="create.php">Create</a>

<?php
error_reporting(E_ALL ^ E_DEPRECATED);
//include_once"../../../../Src/BITM/SEIP50/Mobile/Mobile.php";
include_once"../../../../vendor/autoload.php";

use App\BITM\SEIP114352\Mobile\Mobile;
use App\BITM\SEIP114352\Utility\Utility;

$Mobile = new Mobile();
$Mobile->prepare($_GET);
$OneMobile = $Mobile->show();
?>

<html>
    <head>
        <title>Update | Mobile Models</title>
    </head>
    <body>
        <fieldset>
            <legend>
                Update Mobile Models
            </legend> 
            <form action="update.php" method="POST">
                <label>Update Your Favorite Mobile Models</label><br/>
                <input type="text" name="title" id="title" value="<?php echo $OneMobile['title']; ?>"><br/>
                <input type="submit" value="Save">
                <input type="hidden" name="id" value="<?php echo $OneMobile['id'] ?>">
            </form>
        </fieldset>
    </body>
</html>
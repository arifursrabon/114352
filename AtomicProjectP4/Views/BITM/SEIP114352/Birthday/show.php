<a href="create.php">Create</a>

<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Birthday\Birthday;

session_start();
if(isset($_SESSION['Message'])){
    echo ($_SESSION['Message']);
    unset($_SESSION['Message']);
}
$data = new Birthday();
$data->prepare($_GET);
$Onebirthday= $data->show();
?>
<html>
    <body>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Birthday Date</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Action</th>
            </tr>
              <?php
            if(isset($Onebirthday)&& !empty($Onebirthday)){
            ?>
            <tr>
                <td><?php echo $Onebirthday['id']; ?></td>
                <td><?php echo $Onebirthday['birthday']; ?></td>
                <td><?php echo $Onebirthday['created']; ?></td>
                <td><?php echo $Onebirthday['updated']; ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $Onebirthday['id']; ?>">Edit</a>
                    <a href="trast.php?id=<?php echo $Onebirthday['id']; ?>">Delete</a>
                </td>
            </tr>
            <?php
                }
            else{
            ?>
            <tr>
                <td colspan="5"><?php echo "No available data"; ?></td>
            </tr>
            <?php
            }
            ?>
            
        </table>
        
        
    </body>
</html>

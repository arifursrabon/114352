<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Organization\Organization;

$org = new Organization();
$org->prepare($_POST);
$org->update();
?>
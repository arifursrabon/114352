<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Organization\Organization;
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset ($_SESSION['Message']);
}
$org = new Organization();
$org->prepare($_GET);
$Oneorg = $org->show();
?>
<html>
    <title>Organizations</title>
    <body>
        <fieldset>
            <legend><b>Update Organizations Details</b></legend>
            <form action="update.php" method="POST">
                <label for="tilte">Update Organization Name</label></br>
                <input type="text" name="title" readonly="readonly" id="title"  value="<?php echo $Oneorg['title'] ?>"></br>
                <label for="summary">Update Summary of organization</label></br>
                <textarea cols="50" rows="5" name="summary"><?php echo htmlspecialchars(
                         $Oneorg['summary'], ENT_QUOTES, 'UTF-8') ?></textarea></br>
                <input type="hidden" name="id" value="<?php echo $Oneorg['id']?>">
                <input type="submit" value="Save" name="submit">
            </form>
        </fieldset>
    </body>
    
</html>

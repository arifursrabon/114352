<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Organization\Organization;

session_start();
if(isset($_SESSION['Message'])){
    echo ($_SESSION['Message']);
    unset($_SESSION['Message']);
}

$org = new Organization();
$Allorg= $org->trasted();
?>
<html>
    <title>All Organizations</title>
    <body>
        <table border="1">
            <tr>
                <th>SL</th>
                <th>Organization Name</th>
                <th>Action</th>
            </tr>
            <?php
            if(isset($Allorg)&& !empty($Allorg)){
                $serial = 0;
                foreach ($Allorg as $Oneorg) {
                    $serial++;
            ?>
            <tr>
                <td><?php echo $serial; ?></td>
                <td><?php echo $Oneorg['title']; ?></td>
                <td>
                    <a href="restore.php?id=<?php echo $Oneorg['id']; ?>">Restore</a>
                    <a href="delete.php?id=<?php echo $Oneorg['id']; ?>">Delete</a>
                </td>
            </tr>
            <?php
                }
            }
            else{
            ?>
            <tr>
                <td colspan="3"><?php echo "No available data"; ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
    </body>
</html>

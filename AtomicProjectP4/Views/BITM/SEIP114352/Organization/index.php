<a href="create.php">Create     |</a>
<a href="trasted.php">See Deleted Items</a>

<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Organization\Organization;
session_start();
if(isset($_SESSION['Message'])){
    echo ($_SESSION['Message']);
    unset($_SESSION['Message']);
}
$org = new Organization();
$Allorg= $org->index();
?>
<html>
    <title>All Organizations</title>
    <body>
        <table border="1">
            <tr>
                <th>SL</th>
                <th>Organization Name</th>
                <th>Action</th>
            </tr>
            <?php
            if(isset($Allorg)&& !empty($Allorg)){
                $serial = 0;
                foreach ($Allorg as $Oneorg) {
                    $serial++;
            ?>
            <tr>
                <td><?php echo $serial; ?></td>
                <td><?php echo $Oneorg['title']; ?></td>
                <td>
                    <a href="show.php?id=<?php echo $Oneorg['id']; ?>">Show Details</a>
                    <a href="edit.php?id=<?php echo $Oneorg['id']; ?>">Edit</a>
                    <a href="trast.php?id=<?php echo $Oneorg['id']; ?>">Delete</a>
                </td>
            </tr>
            <?php
                }
            }
            else{
            ?>
            <tr>
                <td colspan="3"><?php echo "No available data"; ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
    </body>
</html>

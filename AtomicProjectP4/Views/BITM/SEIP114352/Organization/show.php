<a href="create.php">Create</a>

<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Organization\Organization;

$org = new Organization();
$org->prepare($_GET);
$Oneorg = $org->show();
?>

<html>
    <title>One Organizations</title>
    <body>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Organization Name</th>
                <th>Location</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Action</th>
                
            </tr>
            <?php
            if(isset($Oneorg)&& !empty($Oneorg)){
            ?>
            <tr>
                <td><?php echo $Oneorg['id']; ?></td>
                <td><?php echo $Oneorg['title']; ?></td>
                <td><?php echo $Oneorg['summary']; ?></td>
                <td><?php echo $Oneorg['created']; ?></td>
                <td><?php echo $Oneorg['updated']; ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $Oneorg['id']; ?>">Edit</a>
                    <a href="trast.php?id=<?php echo $Oneorg['id']; ?>">Delete</a>
                </td>
            </tr>
            <?php
                }
            else{
            ?>
            <tr>
                <td colspan="3"><?php echo "No available data"; ?></td>
            </tr>
            <?php
            }
            ?>
        </table>
    </body>
</html>
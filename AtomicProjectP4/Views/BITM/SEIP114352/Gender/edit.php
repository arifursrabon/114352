<?php

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\Gender\Gender;

$data = new Gender();
$data->prepare($_GET);
$Onedata = $data->show();
?>
<html>
    <title>Gender</title>
    <body>
        <fieldset>
            <legend>Update</legend>
            <form action="update.php" method="POST">
                <label>Update your name</label>
                <input type="text" name="name" value="<?php echo $Onedata['name']?>"><br/>
                <label > Gender </label><br/>
                <input type="radio" name="gender" value="Male"
                    <?php if(preg_match("/Male/", $Onedata['gender'])){ echo "checked"; } else{ echo '';}?>/>Male<br/>
                <input type="radio" name="gender" value="Female"
                     <?php if(preg_match("/Female/", $Onedata['gender'])){ echo "checked";} else{echo '';}?>/>Female<br/>
                <input type="submit" value="Update" name="submit">
                <input type="hidden" name="id" value="<?php echo $Onedata['id']?>">
            </form>
            
        </fieldset>  
    </body>
</html>
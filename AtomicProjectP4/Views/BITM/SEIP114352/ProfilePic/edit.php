

<?php
error_reporting(E_ALL^E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP114352\ProfilePic\ProfilePic;
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$data = new ProfilePic();
$data->prepare($_GET);
$Onedata = $data->show();
?>

<html>
    <title> Picture</title>
    <body>
        <form action="update.php" method="POST" enctype="multipart/form-data">
                <fieldset>
                    <legend>Update</legend>
                    <div>
                        <label for="title">Update your name </label><br/>
                        <input type="text" name="name" id="name" value="<?php echo $Onedata['name']?>" ><br/>
                    </div>
                    <div>
                        <label>Update your picture</label><br/>
                        <img src="../../../../Image/<?php echo $Onedata['image'] ?>"  alt="No image" height="200" width="200" >><br/><br/>
                        <input type="file" name="image" ><br/>
                    </div>
                    <div>
                        <input type="hidden" name="id" value="<?php echo $Onedata['id']?>">
                    </div>
                    <div>
                        <input type="submit" value="Save" >
                    </div>
                </fieldset>
            </form>
  </body>
</html>
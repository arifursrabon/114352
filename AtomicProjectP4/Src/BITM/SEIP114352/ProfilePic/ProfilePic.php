<?php
namespace App\BITM\SEIP114352\ProfilePic;
use PDO;
class ProfilePic {
    public $id = '';
    public $name = '';
    public $image='';
    public $conn='';
    public $dbname='root';
    public $password='';


    public function __construct(){
        try{
            $this->conn=new PDO('mysql:host=localhost;dbname=atomicprojectp4', $this->dbname,  $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e){
            echo 'ERROR ';
        }
 
    }
    
    public function prepare($data = ''){
        if(is_array($data) && array_key_exists('name', $data)){
            $this->name= $data['name'];
        }
        if(is_array($data) && array_key_exists('id', $data)){
            $this->id= $data['id'];
        }
        if(is_array($data) && array_key_exists('image', $data)){
            $this->image= $data['image'];
        }
 
    }
    
    public function store(){
        session_start();

        if(!empty($this->name) && !empty($this->image)){
        $stmt="INSERT INTO `profilepic` (`name`, `image`,`created`)  VALUES (:n,:i,:c)";
        $q=  $this->conn->prepare($stmt);
        $q->execute(array(
            ':n'=>  $this->name,
            ':i'=>  $this->image,
            ':c'=> date('Y-m-d H:i:s'),
                ));
        if ($q) {
            $_SESSION['Message'] = "<h2>Successfully Added</h2>";
        } else {
            $_SESSION['Message'] = "<h2>Opps Somthing going wrong</h2>";
        }
         header("location:index.php");
        }
        else{
            $_SESSION['Message'] = "<h2>Please Enter Something</h2>";
            header('location:create.php');
        }

    }
    public function index(){
     $mydata=array();
     $sql = "SELECT * FROM `profilepic` WHERE deleted_at is NULL ";
     $q=  $this->conn->query($sql) or die('Failed');
     while($row = $q->fetch(PDO::FETCH_ASSOC)){
         $mydata[]= $row;
     }
     return $mydata;
        
    }
    public function trasted(){
     $mydata=array();
     $sql = "SELECT * FROM `profilepic` WHERE deleted_at is NOT NULL ";
      $q=  $this->conn->query($sql) or die('Failed');
     while($row = $q->fetch(PDO::FETCH_ASSOC)){
         $mydata[]= $row;
     }
     return $mydata;
    }
    
    public function trast(){
    
       session_start();
       $stmt = "UPDATE `profilepic` SET `deleted_at`=:d WHERE `profilepic`.`id` =:id" ;
       $q=  $this->conn->prepare($stmt);
       $q->execute(array(
           ':d'=>  date('Y-m-d H:i:s'),
           ':id' =>$this->id
           ));
        $_SESSION['Message']= "<h2>Succesfully deleted</h2>";
        header('location:index.php');
        
    }

    public function show(){
        $stmt = "SELECT * FROM `profilepic` WHERE id=:id";
        $q=  $this->conn->prepare($stmt);
         $q->execute(array(
             ':id'=>  $this->id
                 ));
        $row = $q->fetch(PDO::FETCH_ASSOC);
        return  $row;
    }
    public function delete(){
        session_start();
        $stmt="DELETE FROM `profilepic` WHERE `profilepic`.`id`=:id";
        $q=  $this->conn->prepare($stmt);
        $q->execute(array(
             ':id'=>  $this->id
                 ));
        header('location:index.php');
        $_SESSION['Message']= "<h2>Successfully Deleted</h2>";
    }
    public function update(){
        session_start();
        if(!empty($this->image)){
        $stmt = "UPDATE `profilepic` SET `name` = :n, `image` = :i,`updated`=:u WHERE `profilepic`.`id` =:id";
        $q=  $this->conn->prepare($stmt);
        $q->execute(array(
            ':n'=>  $this->name,
            ':i'=>  $this->image,
            ':u'=>  date('Y-m-d H:i:s'),
            ':id'=>  $this->id,
                ));
        }
        else{
        $stmt = "UPDATE `profilepic` SET `name` = :n,`updated`=:u WHERE `profilepic`.`id` =:id";
        $q=  $this->conn->prepare($stmt);
        $q->execute(array(
            ':n'=>  $this->name,
            ':u'=>  date('Y-m-d H:i:s'),
            ':id'=>  $this->id,
                ));
        }
        if($q){
            $_SESSION['Message']= "<h2>Successfully Updated</h2>";
            header('location:index.php');
        }
        else{
            $_SESSION['Message']= "<h2>Plz write something</h2>";
            header('location:edit.php');
        }
    }
       
    public function restore(){
        session_start();
        $stmt = "UPDATE `profilepic` SET `deleted_at` =:d WHERE `profilepic`.`id` =:id";
        $q=  $this->conn->prepare($stmt);
        $q->execute(array(
           ':d'=>  NULL,
           ':id'=>  $this->id,
           ));
        $_SESSION['Message']= "<h2>Succesfully Restored</h2>";
        header('location:index.php'); 
        
    }
}

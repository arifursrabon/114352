<?php
namespace App\BITM\SEIP114352\Organization;

class Organization {
  
    public $title='';
    public $summary='';
    public $id = '';
    
    public function __construct() {
        mysql_connect("localhost", "root", "") or die('Cannot connect');
        mysql_select_db('AtomicProjectP4') or die('Unnable to connect with DB');
    }


    public function prepare($data=''){
        if(array_key_exists('title', $data)){
            $this->title=$data['title'];
        }
        if(array_key_exists('summary', $data)){
            $this->summary=$data['summary'];
        }
        if(array_key_exists('id', $data)){
            $this->id=$data['id'];
        }
        
    }
    public function store(){
        session_start();
        if(!empty($this->title)&& !empty($this->summary)){
            $query  = "INSERT INTO  `organizations` (`id`, `title`, `summary`, `created`,"
                    . " `updated`, `deleted_at`) VALUES (NULL, '".$this->title."',"
                    . " '".$this->summary."', '".date('Y-m-d H-i-s')."', '".date('Y-m-d H-i-s')."', NULL)";

            if(mysql_query($query)){
                $_SESSION['Message']= "<h3>Succesfully Added</h3>";    
            }
            else{
                $_SESSION['Message']= "<h2>Opps something is wrong</h2>";
            }
            header('location:index.php');
        }
        else{
            $_SESSION['Message']= "<h2>Please write somthing</h2>";
            header('location:create.php');
        }
    }
    public function index(){
        $mydata=array();
        $query = "SELECT * FROM `organizations` WHERE deleted_at is NULL";
        $result = mysql_query($query);
        while($row = mysql_fetch_assoc($result)){
            $mydata[]=$row;
        }
        return $mydata;
    }
    public function show(){
        $query = "SELECT * FROM `organizations` WHERE `id`=".$this->id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
        
    }
    public function update(){
        session_start();
        $query="UPDATE `organizations` SET `title` = '".$this->title."', "
                . "`summary` = '".$this->summary."' WHERE `organizations`.`id` =".$this->id;
        if(!empty($this->title)&& !empty($this->summary)){
        mysql_query($query);
        $_SESSION['Message']= "<h3>Succesfully Updated</h3>";
        header('location:index.php');
        }
        else {
        $_SESSION['Message']="<h2>Plz write something</h2>";
         header('location:edit.php');
        }
    }
    public function trasted(){
        $mydata=array();
        $query = "SELECT * FROM `organizations` WHERE deleted_at is NOT NULL";
        $result = mysql_query($query);
        while($row = mysql_fetch_assoc($result)){
            $mydata[]=$row;
        }
        return $mydata;
        
    }
    public function trast(){
        session_start();
        $query = "UPDATE `organizations` SET `deleted_at` = '".date('Y-m-d H-i-s')."' "
                . "WHERE `organizations`.`id` =".$this->id;
        mysql_query($query);
        $_SESSION['Message']="<h3>Deleted Succesfully</h3>";
        header('location:index.php');
    }
    public function delete(){
        session_start();
        $query = "DELETE FROM `organizations` WHERE `organizations`. `id` =".$this->id;
        mysql_query($query);
        $_SESSION['Message']="<h3>Deleted Successfully</h3>";
        header('location:trasted.php');
        
    }
    public function restore(){
        session_start();
        $query =  "UPDATE `organizations` SET `deleted_at` = NULL "
                . "WHERE `organizations`.`id` =".$this->id;
        mysql_query($query);
        $_SESSION['Message']="<h3>Restore Successfully</h3>";
        header('location:index.php');
    }
         
}

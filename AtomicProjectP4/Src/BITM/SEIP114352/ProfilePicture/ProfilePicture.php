<?php
namespace App\BITM\SEIP114352\ProfilePicture;

class ProfilePicture {
    public $id = '';
    public $name = '';
    public $image='';
    
    public function __construct(){
        mysql_connect("localhost","root", "") or die("cannot connect");
        mysql_select_db('AtomicProjectP4') or die('Unable to connect with DB');
    }
    
    public function prepare($data = ''){
        if(is_array($data) && array_key_exists('name', $data)){
            $this->name= $data['name'];
        }
        if(is_array($data) && array_key_exists('id', $data)){
            $this->id= $data['id'];
        }
        if(is_array($data) && array_key_exists('image', $data)){
            $this->image= $data['image'];
        }
 
    }
    
    public function store(){
        session_start();

        if(!empty($this->name) && !empty($this->image)){
        $query="INSERT INTO `profilepic` (`id`, `name`, `image`, `created`, `updated`, `deleted_at`)"
                . " VALUES (NULL, '" .$this->name. "', '" .$this->image. "', '".  date('Y-m-d H:i:s')."', '".  date('Y-m-d H:i:s')."', NULL)";
        if (mysql_query($query)) {
            $_SESSION['Message'] = "<h2>Successfully Added</h2>";
        } else {
            $_SESSION['Message'] = "<h2>Opps Somthing going wrong</h2>";
        }
        }
        else{
            $_SESSION['Message'] = "<h2>Please Enter Something</h2>";
            header('location:create.php');
        }
        header("location:index.php");
    }
    public function index(){
     $mydata=array();
     $query = "SELECT * FROM `profilepic` WHERE deleted_at is NULL ";
     $result = mysql_query($query);
     while($row = mysql_fetch_assoc($result)){
         $mydata[]= $row;
     }
     return $mydata;
        
    }
    public function trasted(){
     $mydata=array();
     $query = "SELECT * FROM `profilepic` WHERE deleted_at is NOT NULL ";
     $result = mysql_query($query);
     while($row = mysql_fetch_assoc($result)){
         $mydata[]= $row;
     }
     return $mydata;   
    }
    
    public function trast(){
    
        session_start();
        $query = "UPDATE `profilepic` SET `deleted_at` ='".date('Y-m-d')."' WHERE `profilepic`.`id` =".$this->id;
        mysql_query($query);
        $_SESSION['Message']= "<h2>Succesfully deleted</h2>";
        header('location:index.php');
        
    }

    public function show(){
        $query = "SELECT * FROM `profilepic` WHERE id=".$this->id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return  $row;
    }
    public function delete(){
        session_start();
        $query="DELETE FROM `profilepic` WHERE `profilepic`.`id`=".$this->id;
        $result = mysql_query($query);
        header('location:index.php');
        $_SESSION['Message']= "<h2>Successfully Deleted</h2>";
    }
    public function update(){
        session_start();
        if(!empty($this->image)){
        $query = "UPDATE `profilepic` SET `name` = '" .$this->name. "', `image` = '" .$this->image. "' WHERE `profilepic`.`id` =".$this->id;
        if(mysql_query($query)){
            $_SESSION['Message']= "<h2>Successfully Updated</h2>";
            header('location:index.php');
        }
        else{
            $_SESSION['Message']= "<h2>Plz write something</h2>";
            header('location:edit.php');
        }
        }
        else{
        $query = "UPDATE `profilepic` SET `name` = '" .$this->name. "' WHERE `profilepic`.`id` =".$this->id;
        if(mysql_query($query)){
            $_SESSION['Message']= "<h2>Successfully Updated</h2>";
            header('location:index.php');
        }
        else{
            $_SESSION['Message']= "<h2>Something wrong</h2>";
            header('location:edit.php');
        }
        }
        
    }
    public function restore(){
        session_start();
        $query = "UPDATE `profilepic` SET `deleted_at` = NULL WHERE `profilepic`.`id` =".$this->id;
        mysql_query($query);
        $_SESSION['Message']= "<h2>Succesfully Restored</h2>";
        header('location:index.php'); 
        
    }
}

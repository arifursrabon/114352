-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2016 at 06:18 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicprojectp4`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdays`
--

CREATE TABLE `birthdays` (
  `id` int(11) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthdays`
--

INSERT INTO `birthdays` (`id`, `birthday`, `created`, `updated`, `deleted_at`) VALUES
(1, '2011-01-04', '2016-03-26 03:30:30', '2016-03-26 03:30:30', '2016-04-01 12:56:34'),
(2, '1999-10-27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-01 12:56:33'),
(3, '2016-03-29', '2016-03-22 06:01:58', '2016-03-22 06:01:58', '2016-03-22 07:40:55'),
(4, '2016-03-27', '2016-03-22 06:20:25', '2016-03-22 06:20:25', NULL),
(5, '2000-03-30', '2016-03-22 06:37:09', '2016-03-22 06:37:09', '2016-04-01 12:56:35'),
(6, '2016-03-22', '2016-03-22 07:41:58', '2016-03-22 07:41:58', NULL),
(7, '2016-03-22', '2016-03-22 07:54:33', '2016-03-22 07:54:33', '2016-03-22 07:57:42'),
(8, '1991-03-15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(9, '2016-03-23', '2016-03-22 07:56:59', '2016-03-22 07:56:59', NULL),
(10, '2008-01-25', '2016-03-26 04:15:00', '2016-03-26 04:15:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `created`, `updated`, `deleted_at`) VALUES
(1, 'Mecbeth', 'Shekespeer', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Computer Algorithm', 'B H Bell', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(4, 'Computer Architecture', 'A J Khan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(5, 'Shilalipi', 'Ahmed', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-06 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `city`, `created`, `updated`, `deleted_at`) VALUES
(1, 'Sadik ahmed', 'Chuadanga', '2016-03-29 08:21:06', '2016-03-29 08:21:06', NULL),
(2, 'Arifur Rahman', 'Comilla', '2016-03-29 08:34:36', '2016-03-29 08:34:36', NULL),
(3, 'Abir Ahmed', 'Savar', '2016-03-29 08:34:50', '2016-03-29 08:34:50', NULL),
(4, 'Sohel Rana', 'Bogra', '2016-03-29 08:35:03', '2016-03-29 08:35:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `title`, `created`, `updated`, `deleted_at`) VALUES
(1, 'arifur.reza.srabon@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'sadik101294@gmail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'safsg@gamil.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(7, 'bangladesh@lkl.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(8, 'india@lkl.com', '2016-03-24 07:08:32', '2016-03-24 07:08:32', NULL),
(9, 'india@lkl.com', '2016-03-24 07:08:55', '2016-03-24 07:08:55', NULL),
(10, 'sadik101294@gmail.com', '2016-03-24 07:10:08', '2016-03-24 07:10:08', NULL),
(11, 'omarfaroque79@yahoo.com', '2016-03-26 04:15:39', '2016-03-26 04:15:39', NULL),
(12, 'asdf@yahoo.com', '2016-03-31 05:28:45', '2016-03-31 05:28:45', '2016-03-31 00:00:00'),
(13, 'asdf@yahoo.com', '2016-03-31 05:29:38', '2016-03-31 05:29:38', '2016-03-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `created`, `updated`, `deleted_at`) VALUES
(1, 'Arif', 'Male', '2016-03-29 06:19:39', '2016-03-29 06:19:39', NULL),
(2, 'Sadik ahmed', 'Male', '2016-03-29 06:23:20', '2016-03-29 06:23:20', NULL),
(3, 'Abir', 'Male', '2016-03-29 06:24:14', '2016-03-29 06:24:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `created`, `updated`, `deleted_at`) VALUES
(1, 'Cricket,Football,Chase', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(2, 'Cricket,Football,Chase', '2016-03-26 07:37:11', '2016-03-26 07:37:11', NULL),
(3, 'Cricket,Football,Gardening', '2016-03-26 07:51:01', '2016-03-26 07:51:01', NULL),
(4, 'Football,Gardening', '2016-03-26 07:52:35', '2016-03-26 07:52:35', NULL),
(5, 'Cricket,Football,Chase', '2016-03-26 08:04:53', '2016-03-26 08:04:53', NULL),
(7, 'Cricket,Art', '2016-03-26 08:09:50', '2016-03-26 08:09:50', NULL),
(8, 'Cricket,Chase,Art', '2016-03-26 08:10:52', '2016-03-26 08:10:52', NULL),
(9, 'Football', '2016-03-26 08:12:54', '2016-03-26 08:12:54', NULL),
(11, 'Chase', '2016-03-27 07:01:14', '2016-03-27 07:01:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mobiles`
--

CREATE TABLE `mobiles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobiles`
--

INSERT INTO `mobiles` (`id`, `title`, `created`, `updated`, `deleted_at`) VALUES
(11, 'Nokia Lumia 720', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-16 00:00:00'),
(13, 'Oppo F2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(15, 'Walton V9', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(16, 'Symphony H125', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(17, 'Halio H1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(18, 'hfjg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `title`, `summary`, `created`, `updated`, `deleted_at`) VALUES
(1, 'Data Park It Ltd', 'Dhaanmondi, 1206', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(3, 'Royal IT Park', 'Mouchak, Begum Rokeya Sharani', '2016-03-18 17:32:26', '2016-03-18 17:32:26', NULL),
(4, 'Sark Kniteware Ltd', 'Green Road', '2016-03-18 18:28:20', '2016-03-18 18:28:20', NULL),
(5, 'DT', 'Dynamic Table ', '2016-03-26 04:36:06', '2016-03-26 04:36:06', NULL),
(6, 'Fight Org', 'Trainning of fight ', '2016-03-26 04:38:05', '2016-03-26 04:38:05', '2016-04-06 08:52:17'),
(7, 'hghfh', 'ghtyufgnhfghrty', '2016-03-26 04:49:09', '2016-03-26 04:49:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepic`
--

CREATE TABLE `profilepic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepic`
--

INSERT INTO `profilepic` (`id`, `name`, `image`, `created`, `updated`, `deleted_at`) VALUES
(20, 'Abir', '1459835225ipadair_large.jpg', '2016-04-05 07:47:05', '0000-00-00 00:00:00', NULL),
(22, 'Sohel Rana', '1459838407w1.jpeg', '2016-04-05 08:03:36', '2016-04-05 08:40:07', NULL),
(23, 'Arifur Srabon', '1459836840w7.jpg', '2016-04-05 08:14:00', '0000-00-00 00:00:00', '2016-04-05 08:41:01'),
(24, 'Hakim', '1459838394ipadmini_large.jpg', '2016-04-05 08:39:54', '0000-00-00 00:00:00', NULL),
(25, 'Sadik Ahmed', '1459838455compare_large.png', '2016-04-05 08:40:55', '0000-00-00 00:00:00', NULL),
(26, 'Arif', '1459839391w5.jpg', '2016-04-05 08:56:31', '2016-04-05 08:56:31', NULL),
(27, 'Abir', '1459839503ipadpro_large.png', '2016-04-05 08:58:23', '2016-04-05 08:58:23', NULL),
(28, 'Showkat', '1459861593PicsArt_01-02-09.24.34.jpg', '2016-04-05 15:06:33', '0000-00-00 00:00:00', NULL),
(29, 'Arifur Reza Srabon', '1459862057PicsArt_01-02-09.17.54.jpg', '2016-04-05 15:14:17', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `term` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `name`, `term`, `created`, `updated`, `deleted_at`) VALUES
(2, 'Arifur Reza Srabon', 'condition', '2016-04-06 08:30:53', '0000-00-00 00:00:00', NULL),
(3, 'Shameem Ahmed', ' condition ', '2016-04-06 08:35:34', '2016-04-06 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `title`, `password`, `created`, `updated`, `deleted_at`) VALUES
(3, 'Abir Ahmed', '1234567', '2016-03-20 16:26:54', '2016-03-26 04:28:34', NULL),
(4, 'Shohel Rana', '5555555555555', '2016-03-20 16:27:36', '2016-03-20 16:27:36', NULL),
(5, 'Sadik Ahmed', '11111', '2016-03-20 16:28:09', '2016-03-20 16:28:09', NULL),
(8, 'Arifur Reza Srabon', '000000000000', '2016-03-20 16:39:16', '2016-03-20 16:39:16', NULL),
(9, 'Ogni Oporna', '', '2016-03-20 16:39:56', '2016-03-20 16:39:56', NULL),
(10, 'Omar', '1234567890', '2016-03-26 03:23:23', '2016-03-26 03:23:23', NULL),
(11, 'rahim', '123456', '2016-03-26 03:51:36', '2016-03-26 03:51:36', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdays`
--
ALTER TABLE `birthdays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobiles`
--
ALTER TABLE `mobiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepic`
--
ALTER TABLE `profilepic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdays`
--
ALTER TABLE `birthdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `mobiles`
--
ALTER TABLE `mobiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `profilepic`
--
ALTER TABLE `profilepic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

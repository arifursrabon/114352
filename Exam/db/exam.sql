-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2016 at 09:12 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `febprogram`
--

CREATE TABLE IF NOT EXISTS `febprogram` (
`id` int(11) NOT NULL,
  `program` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `febprogram`
--

INSERT INTO `febprogram` (`id`, `program`, `created`, `updated`, `deleted_at`) VALUES
(1, 'Fortan,C++', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-27 09:03:04'),
(2, 'Fortan,C,C++,PHP,Parl', '2016-03-27 07:53:05', '2016-03-27 07:53:05', '2016-03-27 09:03:06'),
(3, 'C,C++', '2016-03-27 08:45:27', '2016-03-27 08:45:27', '2016-03-27 08:53:44'),
(4, 'Fortan,PHP,C#', '2016-03-27 08:45:34', '2016-03-27 08:45:34', '2016-03-27 09:03:07'),
(5, 'C#,Parl,Python', '2016-03-27 08:45:40', '2016-03-27 08:45:40', '2016-03-27 09:03:08'),
(6, 'C,PHP', '2016-03-27 08:45:44', '2016-03-27 08:45:44', '2016-03-27 09:03:10'),
(8, 'C,C++,C#', '2016-03-27 08:45:55', '2016-03-27 08:45:55', '2016-03-27 09:03:12'),
(9, 'C', '2016-03-27 08:56:52', '2016-03-27 08:56:52', '2016-03-27 09:03:12'),
(10, 'C++', '2016-03-27 08:56:55', '2016-03-27 08:56:55', '2016-03-27 09:03:14'),
(11, 'C#', '2016-03-27 08:56:58', '2016-03-27 08:56:58', '2016-03-27 09:03:13'),
(12, 'C,C-Plus', '2016-03-27 09:00:02', '2016-03-27 09:00:02', '2016-03-27 09:03:15'),
(13, 'C-Sharp', '2016-03-27 09:01:02', '2016-03-27 09:01:02', '2016-03-27 09:03:14'),
(14, 'CSharp', '2016-03-27 09:02:06', '2016-03-27 09:02:06', '2016-03-27 09:03:16'),
(15, 'Fortan', '2016-03-27 09:03:19', '2016-03-27 09:03:19', NULL),
(16, 'PHP', '2016-03-27 09:03:23', '2016-03-27 09:03:23', NULL),
(17, 'Parl', '2016-03-27 09:03:27', '2016-03-27 09:03:27', NULL),
(18, 'Python,Java', '2016-03-27 09:03:40', '2016-03-27 09:03:40', NULL),
(19, 'Fortan,PHP,Parl', '2016-03-27 09:03:47', '2016-03-27 09:03:47', NULL),
(20, 'CSharp', '2016-03-27 09:03:57', '2016-03-27 09:03:57', NULL),
(21, 'C,CPlus', '2016-03-27 09:04:01', '2016-03-27 09:04:01', NULL),
(22, 'CSharp', '2016-03-27 09:09:19', '2016-03-27 09:09:19', '2016-03-27 09:10:38'),
(23, 'C', '2016-03-27 09:09:33', '2016-03-27 09:09:33', NULL),
(24, 'CSharp', '2016-03-27 09:10:36', '2016-03-27 09:10:36', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `febprogram`
--
ALTER TABLE `febprogram`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `febprogram`
--
ALTER TABLE `febprogram`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

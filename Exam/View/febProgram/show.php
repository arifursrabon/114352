<?php
include_once '../../vendor/autoload.php';
use App\febProgram\febProgram;

$data= new febProgram;
$data->prepare($_GET);
$OneProgram=$data->show();
?>
<html>
    <body>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Programs</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Action</th>
            </tr>
            <?php
            if(isset($OneProgram)&& !empty($OneProgram)){
            ?>
            <tr>
                <td><?php echo $OneProgram['id'] ?></td>
                <td><?php echo $OneProgram['program'] ?></td>
                <td><?php echo $OneProgram['created'] ?></td>
                <td><?php echo $OneProgram['updated'] ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $OneProgram['id'] ?>">Edit    |</a>
                    <a href="trast.php?id=<?php echo $OneProgram['id'] ?>">Delete</a>
                </td>
            </tr>
            <?php 
            }
            else { 
                ?>
            <tr>
                <td colspan="5"><?php echo "No available data" ?></td>
            </tr>
            <?php
            }
            ?>
                        
        </table>
    </body>
</html>

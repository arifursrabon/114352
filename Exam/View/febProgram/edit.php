<?php

include_once '../../vendor/autoload.php';
use App\febProgram\febProgram;
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
    
}

$data= new febProgram;
$data->prepare($_GET);
$OneProgram=$data->show();
?>

<html>
    <head>
        <title>Update</title>
    </head>
    <fieldset>
        <legend>Update</legend>
        <form  action="update.php" method="POST">
            <label>Update Your favourite programming language</label></br>
            <input type="checkbox" name="program[]" value="Fortan" id="Fortan"
               <?php if(preg_match("/Fortan/", $OneProgram['program'])){ echo "checked";} else{echo '';}?>/>Fortan</br>
            <input type="checkbox" name="program[]" value="Programming_in_C" id="Programming_in_C"
                   <?php if(preg_match("/Programming_in_C/", $OneProgram['program'])){ echo "checked";} else{echo '';}?>/>Programming_in_C</br>
            <input type="checkbox" name="program[]" value="CPlus" id="CPlus" 
                   <?php if(preg_match("/CPlus/", $OneProgram['program'])){ echo "checked";} else{echo '';}?>/>CPlus</br>
            <input type="checkbox" name="program[]" value="PHP" id="PHP"
                   <?php if(preg_match("/PHP/", $OneProgram['program'])){ echo "checked";} else{echo '';}?>/>PHP</br>
            <input type="checkbox" name="program[]" value="CSharp" id="CSharp"
                   <?php if(preg_match("/CSharp/", $OneProgram['program'])){ echo "checked";} else{echo '';}?>/>CSharp</br>
            <input type="checkbox" name="program[]" value="Parl" id="Parl"
                   <?php if(preg_match("/Parl/", $OneProgram['program'])){ echo "checked";} else{echo '';}?>/>Parl</br>
            <input type="checkbox" name="program[]" value="Python" id="Python"
                   <?php if(preg_match("/Python/", $OneProgram['program'])){ echo "checked";} else{echo '';}?>/>Python</br>
            <input type="checkbox" name="program[]" value="Java" id="Java"
                   <?php if(preg_match("/Java/", $OneProgram['program'])){ echo "checked";} else{echo '';}?>/>Java</br>
            <input type="hidden" name="id" value="<?php echo $OneProgram['id']?>">
            <input type="submit" value="Save">
        </form>
        
    </fieldset>
</html>
<?php

session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
    
}

?>

<html>
    <head>
        <title>Create</title>
    </head>
    <fieldset>
        <legend>CRUD    | Checkbox (Multiple)</legend>
        <form  action="store.php" method="POST">
            <label>Your favourite programming language</label></br>
            <input type="checkbox" name="program[]" value="Fortan" id="Fortan">Fortan</br>
            <input type="checkbox" name="program[]" value="Programming_in_C" id="Programming_in_C">Programming_in_C</br>
            <input type="checkbox" name="program[]" value="CPlus" id="CPlus">CPlus</br>
            <input type="checkbox" name="program[]" value="PHP" id="PHP">PHP</br>
            <input type="checkbox" name="program[]" value="CSharp" id="CSharp">CSharp</br>
            <input type="checkbox" name="program[]" value="Parl" id="Parl">Parl</br>
            <input type="checkbox" name="program[]" value="Python" id="Python">Python</br>
            <input type="checkbox" name="program[]" value="Java" id="Java">Java</br>
            <input type="submit" value="Save">
            <input type="reset" value="Reset">
        </form>
        
    </fieldset>
</html>

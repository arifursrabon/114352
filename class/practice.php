<?php


class practice {
    
    private $myname = "";
    private $myid = "";
    
    
    public function getName(){

        echo "<b>Before Protected methode call</b>"."</br>";
        $name= "Name = Arifur Rahman";
        $id="ID = 114352";
        $this->myname = $name;
        $this->myid = $id;
        
        echo $this->myname."</br>";
        echo $this->myid."</br></br>";
        
        $this->getId();
    }
    
    protected function getId(){
        
        echo "<b>After Protected methode call</b>"."</br>";
        echo $this->myname."</br>";
        echo $this->myid;
        
    }
}
?>
<?php

$obj= new practice();
$obj->getName();


?>
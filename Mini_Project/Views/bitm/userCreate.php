<?php

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

?>
<html>
    <head>
    <title> Picture</title>
    </head>
    <body>
        <form action="userStore.php" method="POST" enctype="multipart/form-data">
                <fieldset>
                    <legend>Create User Profile</legend>
                    <div>
                        <label for="fname" >Your First Name :</label>
                        <input type="text" name="fname" id="fname" placeholder="First Name"><br/><br/>
                    </div>
                    <div>
                        <label for="lname" >Your Last Name :</label>
                        <input type="text" name="lname" id="lname" placeholder="Last Name" ><br/><br/>
                    </div>
                    <div>
                        <label for="Pphone" >Personal Phone :</label>
                        <input type="number" name="Pphone" id="Pphone" placeholder="Personal Phone"><br/><br/>
                    </div>
                    <div>
                        <label for="Hphone" >Home Phone :</label>
                        <input type="number" name="Hphone" id="Hphone" placeholder="Home Phone"><br/><br/>
                    </div>
                    <div>
                        <label for="Ophone" >Office Phone :</label>
                        <input type="number" name="Ophone" id="Ophone" placeholder="Office Phone"><br/><br/>
                    </div>
                    <div>
                        <label for="Caddress" >Current Address :</label>
                        <textarea cols="50" rows="5" name="Caddress"  id="Caddress" placeholder="Your current adress"></textarea></br><br/>
                    </div>
                    <div>
                        <label for="Paddress" >Permanent Address :</label>
                        <textarea cols="50" rows="5" name="Paddress" id="Paddress" placeholder="Your permanent adress"></textarea></br><br/>
                    </div>
                    <div>
                        <label>Enter your picture</label>
                        <input type="file" name="image"><br/><br/>
                    </div>
                    <div>
                        <input type="submit" value="Save" >
                        <input type="reset" value="reset">
                    </div>
                </fieldset>
            </form>
  </body>
</html>

<?php

include '../../vendor/autoload.php';
use App\bitm\miniProject;
session_start();

$obj=new miniProject();
$obj->setUsername($_POST['user']);
$data=$obj->checkLogin();
if($data['username']!=$_POST['user']){
    $obj->setPassword($_POST['pass']);
    $obj->setEmail($_POST['email']);
    $obj->register();
}
 else {
    $_SESSION['Message'] = "<h2>User name already taken</h2>";
    header('location:../../register.php');
}
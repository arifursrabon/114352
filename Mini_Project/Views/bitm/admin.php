<?php

include '../../vendor/autoload.php';
use App\bitm\miniProject;
session_start();

$obj = new miniProject();
$Alluser=$obj->admin(); 
print_r($Alluser);
die();
?>
<html>
    <title>All user</title>
    <body>
        <table border="1">
            <tr>
                <th>SL</th>
                <th>User Name</th>
                <th>Action</th>
            </tr>
            <?php
            if(!empty($Alluser)){
                $serial=0;
                foreach ($Alluser as $Oneuser) {
                    $serial++;
            ?>
            <tr>
                <td><?php echo $serial; ?></td>
                <td><?php echo $Oneuser['username'];?></td>
                <td>
                    <a href="enable.php?id=<?php echo $Oneuser['id']?>">Enable</a>
                    <a href="edit.php?id=<?php echo $Oneuser['id']?>">Edit</a>
                    <a href="deleteUser.php?id=<?php echo $Oneuser['id']?>">Delete</a>
                </td>
            </tr>
            <?php
                }
            }
            else{
            ?>
            <tr>
                <td colspan="3">No available data</td>
            </tr>
            <?php
            }
            ?>
            
        </table>
        
    </body>
    
</html>
<?php
include '../../vendor/autoload.php';
use App\bitm\miniProject;
session_start();

if (isset($_FILES['image'])) {
    $errors = array();
    $file_name = time() . $_FILES['image']['name'];
    $file_size = $_FILES['image']['size'];
    $file_tmp = $_FILES['image']['tmp_name'];
    $file_type = $_FILES['image']['type'];
    $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));

    $format = array("jpeg", "jpg", "png");

    if (in_array($file_ext, $format) === false) {
        $errors[] = "extention not allowed, please choose a JPEG or PNG file.";
    }
    if ($file_size > 2097152) {
        $errors[] = "File size must be exactly 2MB.";
    }
    if (empty($errors) == true) {
        move_uploaded_file($file_tmp, "../../Image/" . $file_name);
        $_POST['image'] = $file_name;
    } else {
        print_r($errors);
    }
}
$profile = new miniProject();
$profile->prepare($_POST);
$profile->userStore();
?>

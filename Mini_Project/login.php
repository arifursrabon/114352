<html>
    <head>
        <script type="text/javascript" src="js/jquery-1.12.3.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

        <title>Mini Project</title>
    </head>
    <body>

        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>Login</h1><br>
                <form action="views/bitm/checklogin.php" method="post">
                    <input type="text" name="user" placeholder="Username">
                    <input type="password" name="pass" placeholder="Password">
                    <input type="submit"  class="login loginmodal-submit" value="Login">
                </form>

                <div class="login-help">
                    <a href="register.php">Register</a>
                </div>
            </div>
        </div>
        <div style="text-align: center" ></div>
    </body>
</html>
<?php
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<?php
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>

<html>
    <head>
        <script type="text/javascript" src="js/jquery-1.12.3.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

        <title>Registration</title>
    </head>
    <body>

        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>Registration</h1><br>
                <form action="views/bitm/registration.php" method="post">
                    <input type="text" name="user" placeholder="Username">
                    <input type="password" name="pass" placeholder="Password">
                    <input type="email" name="email" placeholder="Email address">
                    <input type="submit"  class="login loginmodal-submit" value="Register">
                </form>

                <div class="login-help">
                    <a href="login.php">Already Registered?</a>
                </div>
            </div>
        </div>
    </body>
</html>

<?php

namespace App\bitm;
use PDO;
class miniProject {

    public $userName = '';
    public $email = '';
    public $password = '';
    public $id='';
    public $fname='';
    public $lname='';
    public $pphone='';
    public $hphone='';
    public $ophone='';
    public $paddress='';
    public $caddress='';
    public $image='';
    public $dbUser = 'root';
    public $dbPass = '';
    public $conn;

    public function __construct() {
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=usrreg', $this->dbUser, $this->dbPass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR: ';
        }
    }
    public function setUsername($data){
        if(!empty($data)){
            $this->username=$data;
            return $data;
        }
    }
    public function setPassword($data){
        if(!empty($data)){
            $this->password=$data;
            return $data;
        }
    }
    public function setEmail($data){
        if(!empty($data)){
            $this->email=$data;
            return $data;
        }
    }
    public function setId($data){
        if(!empty($data)){
            $this->id=$data;
            return $data;
        }
    }
    public function prepare($data=''){
        if(is_array($data) && array_key_exists('fname', $data)){
            $this->fname= $data['fname'];
        }
        if(is_array($data) && array_key_exists('lname', $data)){
            $this->lname= $data['lname'];
        }
        if(is_array($data) && array_key_exists('Pphone', $data)){
            $this->pphone= $data['Pphone'];
        }
        if(is_array($data) && array_key_exists('Hphone', $data)){
            $this->hphone= $data['Hphone'];
        }
        if(is_array($data) && array_key_exists('Ophone', $data)){
            $this->ophone= $data['Ophone'];
        }
        if(is_array($data) && array_key_exists('Caddress', $data)){
            $this->caddress= $data['Caddress'];
        }
        if(is_array($data) && array_key_exists('Paddress', $data)){
            $this->paddress= $data['Paddress'];
        }
        if(is_array($data) && array_key_exists('image', $data)){
            $this->image= $data['image'];
            
        }
    }

    public function register() {
        session_start();
        $stmt="INSERT INTO `usrreg`.`users` (`username`, `password`, `email`) VALUES (:user,:pass,:email)";
        try{
        if(!empty($this->username) && !empty($this->password) && !empty($this->email)){
            $q=  $this->conn->prepare($stmt);
            $q->execute(array(
            ':user'=>  $this->username,
            ':pass'=>  $this->password,
            ':email'=> $this->email
                ));
        if ($q) {
            $_SESSION['Message'] = "<h2>Successfully registered</h2>";
        }
        else {
            $_SESSION['Message'] = "<h2>Opps Somthing going wrong</h2>";
        }
         header("location:../../register.php");
        }
        else{
            $_SESSION['Message'] = "<h2>Please Enter Something</h2>";
            header('location:../../register.php');
        }
        }
        catch (PDOException $e){
            echo 'ERROR ';
        }

        
    }
    public function checkLogin(){
        $stmt="SELECT * FROM `users` WHERE username=:user";
        $q = $this->conn->prepare($stmt);
        $q->execute(array(
             ':user'=>$this->username,
                 ));
        $row = $q->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

        public function profile() {
        
    }
    public function admin(){
        $stmt="SELECT * FROM `users` WHERE deleted_at is NULL";
        $q=  $this->conn->query($sql) or die('Failed');
        while($row = $q->fetch(PDO::FETCH_ASSOC)){
        $mydata[]= $row;
        }
        return $mydata;
    }
    public function user(){
        $stmt="SELECT * FROM `profiles` WHERE deleted_at is NULL";
        $q=  $this->conn->query($sql) or die('Failed');
        while($row = $q->fetch(PDO::FETCH_ASSOC)){
        $mydata[]= $row;
        }
        return $mydata;
    }
    public function store(){
        session_start();
        $lastid = $this->conn->lastInsertId();
        if(!empty($this->fname) && !empty($this->lname) && !empty($this->image) && !empty($this->caddress) && !empty($this->pphone) && !empty($this->paddress) ){
        $stmt="INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `personal_phone` ,`home_phone`,`office_phone`, current_address`,`permanent_address`,`profile_pic`,`created`)  VALUES (:id,:uid,:c)";
        $q=  $this->conn->prepare($stmt);
        $q->execute(array(
            ':n'=>  $this->name,
            ':i'=>  $this->image,
            ':c'=> date('Y-m-d H:i:s'),
                ));
        if ($q) {
            $_SESSION['Message'] = "<h2>Successfully Added</h2>";
        } else {
            $_SESSION['Message'] = "<h2>Opps Somthing going wrong</h2>";
        }
         header("location:index.php");
        }
        else{
            $_SESSION['Message'] = "<h2>Please Enter Something</h2>";
            header('location:create.php');
        }

    }
    public function active(){
        $stmt="UPDATE `users` `is_active` = '1' WHERE `users`.`id`=:id";
        $q=  $this->conn->prepare($stmt);
        $q->execute(array(
            ':id'=>  $this->id,
                ));
    }
    

}

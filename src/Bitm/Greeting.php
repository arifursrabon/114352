<?php

namespace App\Bitm;

class Greeting{
     
    public $name = " ";
    public $greetings = "Hello World";
    
    function __construct($n) {
        $this->name = $n;
    }

    public function sayHello(){
        return $this->greetings;
    }
    public function getName(){
        return $this->name;
    }
}

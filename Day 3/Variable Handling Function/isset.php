<?php
$var = '';
// this will evaluate to TRUE so the text will be printed.
if(isset($var)){
    echo  "This var is set so i will print";
}
//In the next examples we'll use var_dump to output
//the return value of isset().
$a = "test";
$b = "Another test";
var_dump($a);   //true
var_dump($a, $b);//true

unset($a);

var_dump(isset($a));        //false
var_dump(isset($a, $b));//false

$foo = NULL;
var_dump(isset($foo));  //false

?>
